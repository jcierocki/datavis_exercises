<script>
  import Code from "$lib/ui/CodeBlock.svelte";
  import CC from "$lib/ui/CodeInline.svelte";
  import Card from "$lib/ui/Card.svelte";
  import Ex_1 from "./exercises/ex_1.svelte";
  import Ex_2 from "./exercises/ex_2.svelte";
  import Ex_3 from "./exercises/ex_3.svelte";
  import Ex_4 from "./exercises/ex_4.svelte";
</script>

<h1>Week 7</h1>
<p>
  This week's exercises are all about interactivity. You will update last week's
  gapminder visualization to support animating over the years and letting the
  user select which continent is shown.
</p>

<h5>Exercise 1 (1 pt.)</h5>
<p>
  For this exercise, you have to edit the file <CC
    code="src/routes/week_7/exercises/ex_1.svelte"
  />. Any changes you make to that file should show up below.
</p>
<p>
  HTML provides several types of <a
    target="_blank"
    class="link-secondary"
    href="https://www.w3schools.com/tags/tag_input.asp">input</a
  >
  tags as wel as a dedicated
  <a
    target="_blank"
    class="link-secondary"
    href="https://www.w3schools.com/tags/tag_button.asp">button</a
  >
  tag that you can use to let people interact with your visualisations. Svelte also
  makes it easy to program what each input has to do with the
  <a
    target="_blank"
    class="link-secondary"
    href="https://svelte.dev/docs#template-syntax-element-directives-on-eventname"
    >on-directive</a
  >.
</p>
<p>
  Create two buttons, one that keeps track on the number of times you've clicked
  on it and one to reset the counter. Last week, we used <a
    target="_blank"
    href="https://getbootstrap.com/docs/4.0/components/buttons/"
    class="link-secondary">bootstrap</a
  >
  to style our buttons, you can do the same here.
</p>
<Card><Ex_1 /></Card>

<h5>Exercise 2 (1 pt.)</h5>
<p>
  For this exercise, you have to edit the file <CC
    code="src/routes/week_7/exercises/ex_2.svelte"
  />. Any changes you make to that file should show up below.
</p>
<p>
  Interactivity is where Svelte shines; it automates a lot of processes that we
  want to update when values change. However, it can sometimes be confusing to
  reason about a svelte component. We do not always see at a glance what code
  will be updated when values change. It is important to remember that code in
  the script of a component is evaluated only when the component is constructed.
  Code in the HTML markup is evaluated any time a variable that is mentioned
  within the curly braces changes. If you want your script to re-evaluate
  certain statements when a variable changes, you have to use the <CC
    code="$:"
  /> notation:
</p>
<Code
  language="svelte"
  code={`<\script>
  export let myVariable;
  console.log('This message is printed only once!')
  $: console.log(\`This messages prints every time myVariable changes: \$\{myVariable\}\`);
</\script>
`}
/>
<p>
  Remember that only variables that are mentioned within the scope of the <CC
    code="$:"
  /> statement are monitored for changes! Also know that two reactive statements
  do not cause an infinite loop when they change each other's values:
</p>
<Code
  language="svelte"
  code={`<\script>
  export let counter1=0;
  export let counter2=0;

  const increment1 = (_) => counter1+=1;
  const increment2 = (_) => counter2+=1;

  $: increment1(counter2);
  $: increment2(counter1);
</\script>
`}
/>
<p>
  In this example, if you change the value of <CC code="counter2" /> both counters
  will be incremented once. If you change the value of <CC code="counter1" /> only
  <CC code="counter2" /> will be incremented.
</p>
<p>
  Sometimes, you also need to schedule the evaluation of a function, so that it
  will be evaluated some time in the future. JavaScript's <CC
    language="javascript"
    code="setInterval()"
  /> function can be used for that purpose. You give it a function and a delay-time
  and it will keep on calling your function with your specified pause in between
  calls until you either call <CC
    language="javascript"
    code="clearInterval()"
  /> or close the page.
</p>
<p>
  For this exercise, you have to build a countdown light like they use in F1
  racing. There are five lights that are off initially, then they turn red
  one-by-one from the right to the left. After all the lights are red, they
  switch to green simultaneously. The necessary data arrays are already created.
  Use the <CC code="points" /> arrary as x-coordinates of the circle elements and
  <CC code="index" /> as the index of the left-most light that should be on. If a
  light is on set the opacity to <CC code="1" />, otherwise use <CC
    code="0.3"
  />. You should use the second argument of the each-block to capture the index
  of the current iteration. Write a function that reduces the <CC
    code="index"
  /> every second. Once all lights are on, change color to <CC
    code="darkgreen"
  /> and stop the function from reducing the index further. The animation should
  only start when the visualisation is visible!
  <em
    >Hint: remember how you loaded data when the component was first rendered
    last week!</em
  >
</p>
<Card><Ex_2 /></Card>

<h5>Exercise 3 (1 pt.)</h5>
<p>
  For this exercise, you have to edit the files <CC
    code="src/routes/week_7/exercises/ex_3.svelte"
  />, <CC code="src/routes/week_7/exercises/_ex_3_scatterplot.svelte" />, and <CC
    code="src/routes/week_7/exercises/_ex_3_controls.svelte"
  />. Any changes you make to those file should show up below.
</p>
<p>
  It's time to make the gapminder scatterplot interactive. We have already
  prepared a static version for you. Note that we load the data in <CC
    code="ex_3.svelte"
  /> as you learned last week. At the end of this exercise, we want to be able to
  show all the years in the data as an animation. To do that, you have to add a play-pause
  button, a reset button, and a slider that indicates and controls which year is
  shown. The label of the slider should contain the currently shown year, so it is
  easy to read! Finally, you have to add a drop-down menu which can be used to filter
  which continent's countries are shown.
</p>
<p>
  The visualization uses three components, each with a separate responsibility:
</p>
<ul>
  <li>
    <CC code="_ex_3_scatter.svelte" /> creates a scatter plot of the data it is given,
  </li>
  <li>
    <CC code="_ex_3_controls.svelte" /> contains the interactive elements that control
    which year and continents are shown,
  </li>
  <li>
    <CC code="ex_3.svelte" /> manages the data, e.g., loading and extracting the
    selected year and continents. JavaScript contains a very useful array manipulation
    function that extracts items from an array that meet a certain condition:
    <Code
      language="javascript"
      code="const higherThanFive = array.filter(d => d > 5);"
    />
  </li>
</ul>
<p>So, perform these steps:</p>
<ol>
  <li>
    In <CC code="_ex_3_controls.svelte" />, add component properties for the
    currently selected year and continents. The year should be stored as an
    index into the
    <CC code="data" /> array (i.e., the value 0 corresponds to 1800 and the value
    10 to 1810). Also add the described buttons, slider, and drop-down menu that
    manipulate the value of these component properties. Note, the animation should
    loop-back to the start when it reaches the end. The reset button should stop
    the animation if it is active and set 1800 as the selected year.
    <em>
      Hint: to create the animation, remember how you scheduled a function to be
      executed in an interval in the last exercise!
    </em>
  </li>
  <li>
    In <CC code="ex_3.svelte" />, observe the selected year and continent as
    exposed by <CC code="_ex_3_controls.svelte" /> using a <CC code="bind" />.
    Then, define a function that extracts the selected data using the selected
    year and continents variables. Finally, only pass the selected data to the <CC
      code="_ex_3_scatterplot.svelte"
    /> component.
  </li>
</ol>
<p>
  You should now have an interactive GapMinder visualisation! As in Exercise 1,
  you can use <a
    target="_blank"
    class="link-secondary"
    href="https://getbootstrap.com/docs/4.0/components/forms/">boostrap</a
  >
  to make the input elements look nice.
  <em>
    Hint: does your visualisation break when the slider reaches the end? Then
    you probably made an off-by-one error by allowing the slider's value to
    reach 1 item passed the end of the data array!
  </em>
</p>
<Card><Ex_3 /></Card>

<h5>Exercise 4 (1 pt.)</h5>
<p>
  For this exercise, you have to edit the files <CC
    code="src/routes/week_7/exercises/ex_4.svelte"
  /> and <CC code="src/routes/week_7/exercises/_ex_4_scatter.svelte" />. Any
  changes you make to those file should show up below.
</p>
<p>
  We have prepared a visualization that shows two scatter plots using the <CC
    code="/data/cars-2.csv"
  /> dataset. Note that we construct two <CC code="_ex_4_scatter.svelte" /> components
  using a
  <a
    target="_blank"
    class="link-secondary"
    href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax"
    >spread operator</a
  >
  in the then-block of <CC code="ex_4.svelte" />:
</p>
<Code
  language="svelte"
  code={`<\script>
  import Scatter from './_ex_4_scatter.svelte';
  ...
  const s1 = {
    x: (d) => +d.Horsepower,
    y: (d) => +d.Acceleration,
    xLabel: 'Horsepower',
    yLabel: 'Acceleration'
  };
</\script>
...
    <Scatter {data} {...s1} />
...`}
/>
<p>
  Here, <CC code="s1" />'s properties (<CC code="x" />, <CC code="x" />, <CC
    code="xLabel"
  />, and <CC code="xLabel" />) are passed to the <CC code="Scatter" /> component
  as individual component properties. The <CC code="Scatter" /> component uses them
  to extract the values it has to use for the x- and y-coordinates, and puts the
  labels on their respective axes. This way, we can re-use the <CC
    code="Scatter"
  />
  component for two different scatter plots.
</p>
<p>
  In this exercise, you will add brushing and linking to these two scatter
  plots, such that you can draw a rectangle in one plot, and the points that lie
  within that rectangle get highlighted in both plots. In the case that you draw
  a rectangle in both plots, only the points that lie within both rectangles
  should get highlighted.
</p>
<p>
  d3.js provides the functionality that you need to draw these rectangles in the <a
    target="_blank"
    class="link-secondary"
    href="https://github.com/d3/d3-brush">d3-brush</a
  >
  module. Like, with the axes, calling d3.js' <CC code="brush()" /> function gives
  you a function that takes a <em>selected</em> handle to an svg group element
  and adds the brushing functionality to that element. You can use Svelte's
  <a
    target="_blank"
    class="link-secondary"
    href="https://svelte.dev/docs#template-syntax-element-directives-use-action"
    >actions</a
  >
  to apply the brush to an element, just as we did for the axes. However, without
  configuration, the brush will not do anything. You have tell the brush what it
  has to do when a brush starts, moves, and ends. To do that, you have to call the
  <CC code=".on()" /> function on the brush:
</p>
<Code
  language="javascript"
  code={`brush()
  .on('start', doStart)
  .on('brush', doMove)
  .on('end', doEnd)`}
/>
<p>So, to implement brushing and linking, peform these steps:</p>
<ol>
  <li>
    Define two component properties in <CC code="_ex_4_scatter.svelte" />. One
    that stores an array of boolean values indicating which points are selected
    in both scatter plots and one that stores a similar array for the points
    selected in just the local component.
  </li>
  <li>
    In <CC code="ex_4.svelte" /> use a <CC code="bind" /> to observe the local selections
    of both plots. Then reactively define the global selection so that only points
    that are selected in both plots are set to <CC
      language="javascript"
      code="true"
    />. Pass the global selection as a normal component property to the <CC
      code="_ex_4_scatter.svelte"
    />
    components.
  </li>
  <li>
    We already defined the <CC code="range" /> variable for you that will store the
    range of the selection rectangle if there is an active selection or <CC
      language="javascript"
      code="null"
    /> otherwise. Configure a d3-brush so that the <CC code="selection" /> property
    of the argument it passes to your callbacks is written to our <CC
      code="range"
    /> variable when the brush starts, moves, and ends. The values the brush gives
    are in the coordinate system of the element it is applied to. So, apply the brush
    to the SVG group element that contains our datapoints. This ensures that we do
    not have to deal with the margins of our svg element when we compute whether
    a point lies within the selection rectangle.
  </li>
  <li>
    Since we need to know the x- and y-coordinates of our datapoints to
    determine whether they are within a selection and to plot them on the
    screen, it is a good idea to pre-compute these coordinates in the script of <CC
      code="_ex_4_scatter.svelte"
    />. Define a <CC code="coords" /> variable that stores an <CC
      language="javascript"
      code={`\{x, y\}`}
    /> object for each data point. Use it to plot the circles instead of re-computing
    the x- and y-coordinates in the markup section.
  </li>
  <li>
    Use the <CC code="range" /> and <CC code="coords" /> variable to reactively update
    the values of the local selection such that all data points are set to
    <CC language="javascript" code="true" /> if there is no active selection and
    only the points that lie within the rectangle are set to <CC
      language="javascript"
      code="true"
    /> when there is an active selection.
  </li>
  <li>
    Use the global selection to set both the stroke and fill to <CC
      code="darkgray"
    />, the fill opacity to <CC code="0.3" />, and the stroke opacity to <CC
      code="0.5"
    /> when a point is not selected.
  </li>
</ol>
<p>Now you should have a working brushing and linking implementation!</p>
<Card><Ex_4 /></Card>
